const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);
mix.copyDirectory('resources/img','public/img')
mix.js('resources/js/fontawesome.js','public/js')
mix.js('resources/js/popper.min.js','public/js')
mix.js('resources/js/bootstrap.min.js','public/js')
mix.copy('resources/js/plugins/perfect-scrollbar.min.js','public/js/plugins')
mix.js('resources/js/plugins/smooth-scrollbar.min.js','public/js/plugins')
mix.js('resources/js/soft-ui-dashboard.mine166.js','public/js')
mix.js('resources/js/plugins/chartjs.min.js','public/js/plugins');
mix.postCss('resources/css/nucleo-icons.css', 'public/css');
mix.postCss('resources/css/nucleo-svg.css', 'public/css');
mix.postCss('resources/css/soft-ui-dashboard.mine166.css', 'public/css');

if (mix.inProduction()) {
    mix.version();
}

