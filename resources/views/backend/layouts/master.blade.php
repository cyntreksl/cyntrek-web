<!DOCTYPE html>
<html lang="en">
@include('backend.includes.header')

@stack('styles')

<body class="g-sidenav-show  bg-gray-100">

@include('backend.includes.menu')

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    @include('backend.includes.navbar')

    <div class="container-fluid py-4">
        @yield('content')

        @include('backend.includes.footer')
    </div>
</main>

@include('backend.includes.scripts')

@stack('scripts')

</body>

</html>
