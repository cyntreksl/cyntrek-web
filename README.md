<p align="center"><a href="https://cyntrek.com" target="_blank"><img src="https://cyntrek.com/cyntrek-logo.jpg" width="200"></a></p>



## Cyntrek Admin
Common admin repo for All the cyntrek products since 2022

## Installing

Clone the repo and goto project folder

`cd cyntrek-web`

copy the .env file and change the enviroment values like databse name and others.to copy .env file

`cp .env.example .env`

after changing the environment variable run following command to initialize the project

`php artisan key:generate`

`php artisan migrate`

`php artisan db:seed`

`npm install && npm run dev`

finaly

`php artisan serve`

##Crew Members
- Chinthaka Dilan F
- Kasun Madushanka
- Ishara Sewwandi
- Asanka Kumara
- Yasas Malinga
- Nadee Wickramasinghe
- Nelumi Nimnadie
